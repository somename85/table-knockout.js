function ItemViewModel(item, category, total) {
    var self = this;

    self.name = 'item ' + item.name;
    self.gramms = ko.observable(round(+item.gramms));

    self.cat = ko.computed({
        read: function() {
            return round(+self.gramms() * category.cat/category.gramms());
        },
        write: function(typed) {
            self.gramms(round(+typed * category.gramms()/category.cat));
        }
    });

    self.total = ko.computed({
        read: function() {
            return round(+self.gramms() * total.total/total.gramms());
        },
        write: function(typed) {
            self.gramms(round(+typed * total.gramms()/total.total));
        }
    });
}

function CategoryViewModel(category, total) {
    var self = this;

    self.items = ko.observableArray([]);
    self.name = 'Category ' + category.name;
    self.cat = 100;

    self.gramms = ko.computed({
        read: function() {
            var result = 0;

            self.items().forEach(function(item) {
                result += +item.gramms();
            });

            return round(result);
        },
        write: function(typed) {
            var categoriesPercentages = [];
            
            self.items().forEach(function(item) {
                categoriesPercentages.push(item.cat());
            });

            self.items().forEach(function(item, i) {
                item.gramms(round(+typed/100 * categoriesPercentages[i]));
            })
        }
    });

    self.total = ko.computed({
        read: function() {
            return round(total.total * self.gramms()/total.gramms());
        },
        write: function(typed) {
            //when we changing category total, we can increase current category gramms
            //or reduce other category gramms
            //i choose increase.

            var otherCategoriesGramms   = total.gramms() - self.gramms(),
                otherCategoriesPercent  = total.total - typed,
                onePercent              = otherCategoriesGramms/otherCategoriesPercent,
                currentCategoryGramms   = typed * onePercent;

            self.gramms(round(currentCategoryGramms));
        }
    });

    category.items.forEach(function(item) {
        self.items.push(new ItemViewModel(item, self, total));
    });
}

function TotalViewModel(categories) {
    var self = this;

    self.total = 100;
    self.categories = ko.observableArray([]);

    self.gramms = ko.computed({
        read: function() {
            var result = 0;

            self.categories().forEach(function(category) {
                result += category.gramms();
            });

            return round(result);
        },
        write: function(typed) {
            var totalPercentages = [];

            self.categories().forEach(function(category) {
                totalPercentages.push(category.total());
            });

            self.categories().forEach(function(category, i) {
                category.gramms(round(+typed/100 * totalPercentages[i]));
            });
        }
    });

    categories.forEach(function(category) {
        self.categories.push(new CategoryViewModel(category, self));
    });
}



function round(num) {
    return Math.round(num * 100000000) / 100000000;
}