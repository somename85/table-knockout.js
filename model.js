/**
 * Created by blackbookman on 09.04.15.
 */
var categories = [
    {
        "name": "#1",
        "items": [
            {
                "name": "#1",
                "gramms": 7
            },
            {
                "name": "#2",
                "gramms": 3.5
            },
            {
                "name": "#3",
                "gramms": 4.6
            }
        ]
    },
    {
        "name": "#2",
        "items": [
            {
                "name": "#1",
                "gramms": 74
            },
            {
                "name": "#2",
                "gramms": 5
            },
            {
                "name": "#3",
                "gramms": 9
            }
        ]
    },
    {
        "name": "#2",
        "items": [
            {
                "name": "#1",
                "gramms": 0.5
            },
            {
                "name": "#2",
                "gramms": 7.1
            },
            {
                "name": "#3",
                "gramms": 92
            }
        ]
    }
];